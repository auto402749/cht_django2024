from django.db import models
from django.utils import timezone

class Post(models.Model):
    title = models.CharField(max_length = 200)
    slug = models.CharField(max_length = 200)
    body = models.TextField()
    pub_date = models.DateTimeField(default = timezone.now)


    def __str__(self):
        return self.title
    
    class Meta:
        ordering = ['-pub_date']
        verbose_name  ='用戶資料庫'
        verbose_name_plural = '用戶資料庫'
